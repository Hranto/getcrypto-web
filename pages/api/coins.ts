import {NextApiRequest, NextApiResponse} from 'next';

import {coinGeckoClient} from '~/services';

const getCoins = async (
  req: NextApiRequest,
  res: NextApiResponse,
): Promise<void> => {
  try {
    const coins = await coinGeckoClient.coinMarket({
      page: 1,
      ids: '',
      per_page: 10,
      vs_currency: 'usd',
      order: 'market_cap_desc',
    });

    res.status(200).json(coins);
  } catch (e) {
    res.status(400).json({message: 'Something went wrong'});
  }
};

export default getCoins;
