import {NextApiRequest, NextApiResponse} from 'next';

import {coinGeckoClient} from '~/services';

const getCoinChart = async (
  req: NextApiRequest,
  res: NextApiResponse,
): Promise<void> => {
  try {
    const coinChart = await coinGeckoClient.coinIdMarketChart({
      vs_currency: 'usd',
      id: req.query.id as string,
      days: Number(req.query.days),
    });

    res.status(200).json(coinChart);
  } catch (e) {
    res.status(400).json({message: 'Something went wrong'});
  }
};

export default getCoinChart;
