import {NextApiRequest, NextApiResponse} from 'next';

import {coinGeckoClient} from '~/services';

const getCoin = async (
  req: NextApiRequest,
  res: NextApiResponse,
): Promise<void> => {
  try {
    const coin = await coinGeckoClient.coinId({
      id: req.query.id as string,
    });

    if (!coin.id) {
      res.status(404).json({message: 'Not Found'});
    } else {
      res.status(200).json(coin);
    }
  } catch (e) {
    res.status(400).json({message: 'Something went wrong'});
  }
};

export default getCoin;
