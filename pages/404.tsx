import {NextPage} from 'next';

import {SeoLayout} from '~/components';
import {ErrorContainer} from '~/containers';
import {ErrorMetaSettings} from '~/constants';

const ErrorPage: NextPage = () => (
  <SeoLayout
    title={ErrorMetaSettings.title}
    metaDescription={ErrorMetaSettings.description}>
    <ErrorContainer />
  </SeoLayout>
);

export default ErrorPage;
