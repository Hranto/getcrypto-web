import React from 'react';
import {AppProps} from 'next/app';

import '~/styles/index.scss';

import store from '~/redux';

const GetCryptoApp: React.FC<AppProps> = ({Component, pageProps}) => (
  <Component {...pageProps} />
);

export default store.withRedux(GetCryptoApp);
