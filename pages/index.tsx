import {NextPage} from 'next';

import store from '~/redux';
import {SeoLayout} from '~/components';
import {HomeContainer} from '~/containers';
import {HomeMetaSettings} from '~/constants';
import {exchangeInfoActions} from '~/redux/slices/exchangeInfo';

const HomePage: NextPage = () => (
  <SeoLayout
    title={HomeMetaSettings.title}
    metaDescription={HomeMetaSettings.description}>
    <HomeContainer />
  </SeoLayout>
);

export const getStaticProps = store.getStaticProps((store) => async () => {
  await store.dispatch(exchangeInfoActions.getExchangeInfo());

  return {
    props: {},
  };
});

export default HomePage;
