import React from 'react';
import {GetStaticPaths, NextPage} from 'next';
import {CoinFullInfo} from 'coingecko-api-v3';

import store from '~/redux';
import {SeoLayout} from '~/components';
import {CoinContainer} from '~/containers';
import {exchangeInfoActions} from '~/redux/slices/exchangeInfo';

export type CoinPageParams = {
  coinId: string;
};

export type CoinPageProps = Pick<CoinFullInfo, 'name' | 'description'>;

const CoinPage: NextPage<CoinPageProps> = ({name = '', description = {}}) => (
  <SeoLayout
    title={name}
    className="section__coin"
    metaDescription={description.en}>
    <CoinContainer />
  </SeoLayout>
);

export const getStaticPaths: GetStaticPaths<CoinPageParams> = async () => {
  return {
    paths: [],
    fallback: 'blocking',
  };
};

export const getStaticProps = store.getStaticProps(
  (store) =>
    async ({params}) => {
      if (params && params.coinId && typeof params.coinId === 'string') {
        const res = await store.dispatch(
          exchangeInfoActions.getCoin(params.coinId),
        );

        if (!res.payload) {
          return {
            notFound: true,
          };
        }

        const {name, description} = res.payload as CoinFullInfo;

        return {
          props: {
            name,
            description,
          },
        };
      }

      return {
        notFound: true,
      };
    },
);

export default CoinPage;
