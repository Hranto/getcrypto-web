import {makeStore} from './store';

export type AppStore = ReturnType<typeof makeStore>;

export type AppState = ReturnType<AppStore['getState']>;

export enum Reducer {
  EXCHANGE_INFO = 'exchangeInfo',
}
