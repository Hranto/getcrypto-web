import {configureStore} from '@reduxjs/toolkit';
import {createWrapper} from 'next-redux-wrapper';

import {EnvVars} from '~/constants';

import slices from './slices';
import {AppStore} from './types';

const configuredStore = configureStore({
  reducer: slices,
  devTools: !EnvVars.IsProduction,
});

export const makeStore = (): typeof configuredStore => configuredStore;

const store = createWrapper<AppStore>(makeStore, {
  debug: !EnvVars.IsProduction && !EnvVars.IsServer,
});

export default store;
