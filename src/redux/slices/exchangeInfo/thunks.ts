import {createAsyncThunk} from '@reduxjs/toolkit';
import {CoinFullInfo, CoinMarketChartResponse} from 'coingecko-api-v3';

import {Reducer} from '~/redux';
import {IntervalVars} from '~/constants';
import {ExchangeInfoService} from '~/services';

import {Coins, GetCoinChartDataAction} from './types';

export const getExchangeInfo = createAsyncThunk<Coins>(
  `${Reducer.EXCHANGE_INFO}/get`,
  async (credentials, thunkAPI) => {
    try {
      return await ExchangeInfoService.getCoins();
    } catch (error) {
      const {message} = error as Error;

      return thunkAPI.rejectWithValue({error: message});
    }
  },
);

export const getCoinChart = createAsyncThunk<
  CoinMarketChartResponse,
  GetCoinChartDataAction
>(`${Reducer.EXCHANGE_INFO}/coin/chart/get`, async ({id, days}) => {
  try {
    return await ExchangeInfoService.getCoinChart(id, days);
  } catch (error) {
    const {message} = error as Error;

    throw new Error(message);
  }
});

export const getCoin = createAsyncThunk<CoinFullInfo, string>(
  `${Reducer.EXCHANGE_INFO}/coin/get`,
  async (id, {dispatch}) => {
    try {
      const coinResponse = await ExchangeInfoService.getCoin(id);

      await dispatch(getCoinChart({id, days: IntervalVars[0].value}));

      return coinResponse;
    } catch (error) {
      const {message} = error as Error;

      throw new Error(message);
    }
  },
);
