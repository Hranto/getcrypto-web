import {createSelector} from '@reduxjs/toolkit';
import {CoinFullInfo, CoinMarketChartResponse} from 'coingecko-api-v3';

import {AppState} from '~/redux';

const selectExchangeInfo = (state: AppState) => state.exchangeInfo;

export const selectCoins = createSelector(
  selectExchangeInfo,
  (exchangeInfoState) => exchangeInfoState.coins,
);

export const selectCoin = createSelector(
  selectExchangeInfo,
  (exchangeInfoState) => exchangeInfoState.coin as CoinFullInfo,
);

export const selectCoinChart = createSelector(
  selectExchangeInfo,
  (exchangeInfoState) => exchangeInfoState.coinChart as CoinMarketChartResponse,
);
