import exchangeInfoSlice from './slice';
import * as exchangeInfoThunks from './thunks';
import * as exchangeInfoSelector from './selectors';

export const exchangeInfoActions = {
  ...exchangeInfoThunks,
  ...exchangeInfoSlice.actions,
};

export const exchangeInfoSelectors = {
  ...exchangeInfoSelector,
};

export default exchangeInfoSlice.reducer;
