import {HYDRATE} from 'next-redux-wrapper';
import {AnyAction, createSlice} from '@reduxjs/toolkit';

import {Reducer} from '~/redux';

import {
  ExchangeInfoStates,
  GetCoinActionPayload,
  ExchangeInfoSliceState,
  GetCoinChartActionPayload,
  GetExchangeInfoActionPayload,
} from './types';
import * as exchangeInfoThunks from './thunks';

const internalInitialState: ExchangeInfoSliceState = {
  coins: [],
  coin: null,
  error: null,
  coinChart: null,
  loading: ExchangeInfoStates.IDLE,
};

const exchangeInfoSlice = createSlice({
  name: Reducer.EXCHANGE_INFO,
  initialState: internalInitialState,
  reducers: {
    reset: () => internalInitialState,
  },
  extraReducers: (builder) => {
    builder.addCase(HYDRATE, (state, action: AnyAction) => ({
      ...state,
      ...action.payload[Reducer.EXCHANGE_INFO],
    }));

    builder.addCase(
      exchangeInfoThunks.getCoin.fulfilled,
      (state, action: GetCoinActionPayload) => {
        state.error = null;
        state.coin = action.payload;
        state.loading = ExchangeInfoStates.IDLE;
      },
    );

    builder.addCase(
      exchangeInfoThunks.getCoinChart.fulfilled,
      (state, action: GetCoinChartActionPayload) => {
        state.error = null;
        state.coinChart = action.payload;
        state.loading = ExchangeInfoStates.IDLE;
      },
    );

    builder.addCase(exchangeInfoThunks.getExchangeInfo.pending, (state) => {
      state.loading = ExchangeInfoStates.LOADING;
    });
    builder.addCase(
      exchangeInfoThunks.getExchangeInfo.fulfilled,
      (state, action: GetExchangeInfoActionPayload) => {
        state.error = null;
        state.coins = action.payload;
        state.loading = ExchangeInfoStates.IDLE;
      },
    );
    builder.addCase(
      exchangeInfoThunks.getExchangeInfo.rejected,
      (state, action) => {
        state.loading = ExchangeInfoStates.IDLE;
        state.error = action.error;
      },
    );
  },
});

export default exchangeInfoSlice;
