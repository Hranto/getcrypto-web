import {
  CoinMarket,
  CoinFullInfo,
  CoinMarketChartResponse,
} from 'coingecko-api-v3';
import {PayloadAction, SerializedError} from '@reduxjs/toolkit';

export enum ExchangeInfoStates {
  IDLE = 'idle',
  LOADING = 'loading',
}

export type Coins = CoinMarket[];

export type ExchangeInfoSliceState = {
  coins: Coins;
  coin: CoinFullInfo | null;
  loading: ExchangeInfoStates;
  error?: SerializedError | null;
  coinChart: CoinMarketChartResponse | null;
};

// Payloads
export type GetCoinActionPayload = PayloadAction<CoinFullInfo>;

export type GetExchangeInfoActionPayload = PayloadAction<Coins>;

export type GetCoinChartActionPayload = PayloadAction<CoinMarketChartResponse>;

// Actions
export type GetCoinChartDataAction = {
  id: string;
  days: number;
};
