import {combineReducers} from '@reduxjs/toolkit';

import exchangeInfo from './exchangeInfo';

const slices = combineReducers({
  exchangeInfo,
});

export default slices;
