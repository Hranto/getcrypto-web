import React, {useCallback, useEffect, useMemo, useState} from 'react';
import {
  VictoryAxis,
  VictoryChart,
  VictoryLine,
  VictoryTooltip,
  VictoryVoronoiContainer,
} from 'victory';
import numeral from 'numeral';
import {format} from 'date-fns';
import classNames from 'classnames';
import {useDispatch} from 'react-redux';

import {
  exchangeInfoActions,
  exchangeInfoSelectors,
} from '~/redux/slices/exchangeInfo';
import {Button} from '~/components';
import {IntervalVars} from '~/constants';
import {useAppSelector, useInterval} from '~/hooks';

import styles from './CoinChart.module.scss';

const CoinChart: React.FC = () => {
  const dispatch = useDispatch();
  const {id, name} = useAppSelector(exchangeInfoSelectors.selectCoin);
  const {prices} = useAppSelector(exchangeInfoSelectors.selectCoinChart);

  const [dataInterval, setDataInterval] = useState<number>(
    IntervalVars[0].value,
  );

  const data = useMemo(
    () =>
      prices?.map((item) => ({
        x: item[0],
        y: item[1],
      })),
    [prices],
  );

  const updateCoinChart = useCallback(() => {
    dispatch(
      exchangeInfoActions.getCoinChart({id: id as string, days: dataInterval}),
    );
  }, [dataInterval, dispatch, id]);

  useInterval(updateCoinChart, 60000);

  useEffect(() => {
    updateCoinChart();
  }, [updateCoinChart]);

  const intervalButtons = useMemo(
    () =>
      IntervalVars.map((interval) => {
        const intervalButtonClasses = classNames(
          styles.container__actions__interval,
          {
            [styles.container__actions__interval_active]:
              interval.value === dataInterval,
          },
        );

        return (
          <Button
            key={interval.value}
            className={intervalButtonClasses}
            onClick={() => setDataInterval(interval.value)}>
            {interval.label}
          </Button>
        );
      }),
    [dataInterval],
  );

  const victoryVoronoiLabels = ({datum}: {datum: {y: number}}) =>
    numeral(datum.y).format('$0,0[.]00');

  const victoryVoronoiTitle = useMemo(() => `${name} price data chart`, [name]);

  const formatTick = useCallback(
    (x) => {
      if (dataInterval === 1) {
        return format(x, 'p');
      }

      return format(x, 'MM/dd');
    },
    [dataInterval],
  );

  return (
    <div className={styles.container}>
      <div className={styles.container__actions}>{intervalButtons}</div>

      <VictoryChart
        width={900}
        height={400}
        domainPadding={5}
        containerComponent={
          <VictoryVoronoiContainer
            title={victoryVoronoiTitle}
            labels={victoryVoronoiLabels}
            labelComponent={
              <VictoryTooltip
                style={{
                  fill: '#333',
                  fontSize: 16,
                }}
                flyoutStyle={{
                  fill: '#fff',
                  stroke: '#fff',
                  strokeWidth: 1,
                  margin: 10,
                }}
              />
            }
          />
        }>
        <VictoryLine
          style={{
            data: {
              stroke: '#fff',
              strokeWidth: 2,
            },
          }}
          data={data}
        />
        <VictoryAxis
          orientation="bottom"
          style={{
            axis: {
              stroke: '#fff',
              strokeWidth: 2,
            },
            tickLabels: {
              fill: '#fff',
            },
          }}
          tickFormat={formatTick}
        />
      </VictoryChart>
    </div>
  );
};

export default CoinChart;
