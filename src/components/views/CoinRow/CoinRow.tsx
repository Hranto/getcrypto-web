import React, {useCallback, useMemo} from 'react';
import Image from 'next/image';
import numeral from 'numeral';
import {CoinMarket} from 'coingecko-api-v3';

import {Routes} from '~/constants';
import {Link, Typography} from '~/components';

import styles from './CoinRow.module.scss';

const CoinRow: React.FC<CoinMarket> = (props) => {
  const {image, symbol, name, id, ...rest} = props;

  const details = useMemo(
    () => [
      'image',
      'symbol',
      'current_price',
      'total_volume',
      'price_change_percentage_24h',
      'market_cap',
    ],
    [],
  );

  const getFormattedDetail = useCallback(
    (detailType: string, detailValue: number) => {
      switch (detailType) {
        case 'market_cap':
        case 'current_price':
        case 'total_volume': {
          return numeral(detailValue).format('$0,0[.]00');
        }
        case 'price_change_percentage_24h': {
          return numeral(detailValue / 100).format('+0.00%');
        }
        default: {
          return detailValue;
        }
      }
    },
    [],
  );

  const renderDetails = useMemo(
    () =>
      details.map((detail) => {
        let content = null;

        switch (detail) {
          case 'image': {
            content = (
              <>
                {image && (
                  <Image width={40} src={image} height={40} alt={symbol} />
                )}
                <Typography
                  type="medium"
                  variant="button"
                  className={styles.container__coin__heading}>
                  {name}
                </Typography>
              </>
            );
            break;
          }
          case 'symbol': {
            content = (
              <Typography
                type="large"
                variant="heading"
                className={styles.container__coin__heading}>
                {symbol?.toUpperCase()}
              </Typography>
            );
            break;
          }

          default: {
            const formattedDetail = getFormattedDetail(
              detail,
              rest[detail as keyof typeof rest] as number,
            );

            content = (
              <Typography
                type="medium"
                variant="button"
                className={styles.container__coin__heading}>
                {formattedDetail}
              </Typography>
            );
          }
        }

        return (
          <div key={detail} className={styles.container__coin}>
            {content}
          </div>
        );
      }),
    [details, getFormattedDetail, image, name, rest, symbol],
  );

  return (
    <Link to={`${Routes.CoinId}/${id}`} className={styles.container}>
      {renderDetails}
    </Link>
  );
};

export default CoinRow;
