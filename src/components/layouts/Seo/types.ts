export type SeoLayoutProps = {
  title: string;
  className?: string;
  metaDescription: string;
};
