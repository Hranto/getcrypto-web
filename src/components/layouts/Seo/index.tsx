import React, {useMemo} from 'react';
import {DefaultSeo} from 'next-seo';
import classNames from 'classnames';
import {useRouter} from 'next/router';

import {DefaultMetaSettings, Routes} from '~/constants';

import {SeoLayoutProps} from './types';

const SeoLayout: React.FC<SeoLayoutProps> = ({
  title,
  children,
  metaDescription,
  className = '',
}) => {
  const router = useRouter();

  const sectionClasses = useMemo(
    () =>
      classNames('section', {
        [className]: className,
      }),
    [className],
  );

  const asPath = router.asPath === Routes.Home ? '' : router.asPath;

  const url = useMemo(
    () => `${DefaultMetaSettings.domain}/${asPath}`,
    [asPath],
  );

  const openGraph = useMemo(
    () => ({
      url,
      type: 'website',
      site_name: DefaultMetaSettings.name,
      description: DefaultMetaSettings.description,
    }),
    [url],
  );

  return (
    <section className={sectionClasses}>
      <DefaultSeo
        title={title}
        canonical={url}
        openGraph={openGraph}
        description={metaDescription}
        defaultTitle={DefaultMetaSettings.name}
        titleTemplate={`%s | ${DefaultMetaSettings.name}`}
      />

      {children}
    </section>
  );
};

export default SeoLayout;
