import React from 'react';

import {RouteValues} from '~/constants';

type Size = 'large' | 'medium' | 'small';

type Variant = 'primary' | 'secondary' | 'ghost';

type ButtonType = 'submit' | 'reset' | 'button';

export type ButtonProps = {
  size?: Size;
  type?: ButtonType;
  variant?: Variant;
  route?: RouteValues;
  disabled?: boolean;
  className?: string;
  onClick?: () => void;
  LeftIcon?: React.FC<React.SVGProps<SVGSVGElement>>;
};
