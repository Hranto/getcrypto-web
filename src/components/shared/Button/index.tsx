import React, {useCallback} from 'react';
import classNames from 'classnames';

import {RouterService} from '~/services';

import {ButtonProps} from './types';
import styles from './Button.module.scss';

const Button: React.FC<ButtonProps> = ({
  route,
  onClick,
  children,
  LeftIcon,
  disabled,
  size = 'medium',
  type = 'button',
  className = '',
  variant = 'primary',
}) => {
  const buttonClasses = classNames(
    styles.container,
    styles[`container_${size}`],
    styles[`container_${variant}`],
    {
      [className]: className,
      [styles.container__left]: !!LeftIcon,
    },
  );

  const handleLinkClick = useCallback(() => {
    if (route) {
      RouterService.push(route);
    }
  }, [route]);

  return (
    <button
      type={type}
      disabled={disabled}
      className={buttonClasses}
      onClick={route ? handleLinkClick : onClick}>
      {!!LeftIcon && <LeftIcon />}

      {children}
    </button>
  );
};

export default Button;
