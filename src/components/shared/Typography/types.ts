type Align = 'right' | 'left' | 'center' | 'justify';

type Variant = 'text' | 'heading' | 'button' | 'label' | 'link';

type TypoType = 'small' | 'large' | 'extra' | 'medium' | 'semibold' | 'regular';

export type TypographyProps = {
  align?: Align;
  type?: TypoType;
  variant?: Variant;
  className?: string;
  tagName?: keyof JSX.IntrinsicElements;
};
