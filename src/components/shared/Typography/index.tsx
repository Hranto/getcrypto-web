import React from 'react';
import classNames from 'classnames';

import {TypographyProps} from './types';

const Typography: React.FC<TypographyProps> = ({
  children,
  tagName = 'span',
  variant = 'text',
  type = 'regular',
  align = 'left',
  className = '',
  ...rest
}) => {
  const alignKey = `typography__align_${align}`;
  const fontKey = `typography__${variant}_${type}`;

  const classes = classNames(alignKey, fontKey, {
    [className]: className,
  });

  const Tag = tagName;

  return (
    <Tag {...rest} className={classes}>
      {children}
    </Tag>
  );
};

export default Typography;
