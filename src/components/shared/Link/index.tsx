import React from 'react';
import NextLink from 'next/link';
import classNames from 'classnames';
import {useRouter} from 'next/router';

import {LinkProps} from './types';
import styles from './Link.module.scss';

const Link: React.FC<LinkProps> = ({
  to,
  blank,
  onClick,
  children,
  disabled,
  className,
  queryValue,
  anchorProps,
  queryKey = '',
  activeClassName = '',
  ...linkProps
}) => {
  const {asPath, query} = useRouter();

  const anchorModifiedProps = blank
    ? {
        ...anchorProps,
        target: '_blank',
        rel: 'noreferrer',
      }
    : anchorProps;
  const activeClasses = query[queryKey]
    ? query[queryKey] === queryValue
    : asPath.includes(to);

  const anchorClasses = classNames(className, {
    [styles.container_disabled]: disabled,
    [styles.container_active]: activeClasses,
    [activeClassName]: activeClasses && activeClassName,
  });

  return (
    <NextLink href={to} {...linkProps}>
      <a
        role="button"
        onClick={onClick}
        className={anchorClasses}
        {...anchorModifiedProps}>
        {children}
      </a>
    </NextLink>
  );
};

export default Link;
