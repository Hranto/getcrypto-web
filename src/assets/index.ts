// Icons
export {default as BackIcon} from './icons/back.svg';

// Images
export {default as ProfileImage} from './images/profile.png';
