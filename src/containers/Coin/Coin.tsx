import React, {useMemo} from 'react';
import Image from 'next/image';
import numeral from 'numeral';

import {BackIcon} from '~/assets';
import {Routes} from '~/constants';
import {useAppSelector} from '~/hooks';
import {Button, CoinChart, Typography} from '~/components';
import {exchangeInfoSelectors} from '~/redux/slices/exchangeInfo';

import styles from './Coin.module.scss';

const CoinContainer: React.FC = () => {
  const {
    name,
    image,
    market_data = {},
  } = useAppSelector(exchangeInfoSelectors.selectCoin);

  const {current_price, price_change_percentage_24h = 0} = market_data;

  const currentPrice = useMemo(
    () => numeral(current_price?.usd).format('$0,0[.]00'),
    [current_price?.usd],
  );

  const percentage = useMemo(
    () => numeral(price_change_percentage_24h / 100).format('+0.00%'),
    [price_change_percentage_24h],
  );

  return (
    <div className="section__container">
      <div className={styles.content}>
        <div className={styles.content__back}>
          <Button
            route={Routes.Home}
            LeftIcon={BackIcon}
            className={styles.content__back__button}>
            Back
          </Button>
        </div>

        <div className={styles.content__box}>
          <div>
            {image?.large && (
              <Image src={image?.large} alt={name} width={75} height={75} />
            )}
          </div>
          <div className={styles.content__box__name}>
            <Typography variant="heading" type="extra">
              {name}
            </Typography>
          </div>
        </div>

        <div className={styles.content__box__currencies}>
          <div className={styles.content__box__currencies__first}>
            {currentPrice}
          </div>
          <div className={styles.content__box__currencies__second}>
            {percentage}
          </div>
        </div>

        <CoinChart />
      </div>
    </div>
  );
};

export default CoinContainer;
