import React from 'react';

import {Routes} from '~/constants';
import {Button, Typography} from '~/components';

import styles from './Error.module.scss';

const ErrorContainer: React.FC = () => (
  <div className={`section__container ${styles.container}`}>
    <Typography className={styles.container__title}>🧐 404 🤕 </Typography>

    <Button
      size="large"
      className={styles.container__button}
      route={Routes.Home}>
      Go Market
    </Button>
  </div>
);

export default ErrorContainer;
