export {default as CoinContainer} from './Coin';
export {default as HomeContainer} from './Home';
export {default as ErrorContainer} from './Error';
