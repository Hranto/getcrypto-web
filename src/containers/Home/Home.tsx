import React, {useMemo} from 'react';
import Image from 'next/image';
import classNames from 'classnames';
import {useDispatch} from 'react-redux';

import {
  exchangeInfoActions,
  exchangeInfoSelectors,
} from '~/redux/slices/exchangeInfo';
import {ProfileImage} from '~/assets';
import {CoinRow, Typography} from '~/components';
import {useAppSelector, useInterval, useSticky} from '~/hooks';

import styles from './Home.module.scss';

const HomeContainer: React.FC = () => {
  const dispatch = useDispatch();
  const coins = useAppSelector(exchangeInfoSelectors.selectCoins);
  const {stickyContainerRef, isSticky} = useSticky<HTMLDivElement>();

  const titleClasses = useMemo(
    () =>
      classNames(styles.coins__title, {
        [styles.coins__title_sticky]: isSticky,
      }),
    [isSticky],
  );

  const renderCoins = useMemo(
    () => coins.map(({id, ...rest}) => <CoinRow key={id} id={id} {...rest} />),
    [coins],
  );

  useInterval(async () => {
    await dispatch(exchangeInfoActions.getExchangeInfo());
  }, 10000);

  return (
    <div className="section__container">
      <div className={titleClasses}>
        <Typography type="extra" variant="heading">
          Market
        </Typography>

        <Image src={ProfileImage} width={50} height={50} alt="profile" />
      </div>

      <div ref={stickyContainerRef} className={styles.coins}>
        {renderCoins}
      </div>
    </div>
  );
};

export default HomeContainer;
