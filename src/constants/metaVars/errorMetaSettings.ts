const ErrorMetaSettings = {
  title: '404 - Not found',
  description: 'Page not found',
} as const;

export default ErrorMetaSettings;
