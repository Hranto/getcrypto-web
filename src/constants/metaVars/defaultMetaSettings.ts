const DefaultMetaSettings = {
  name: 'Get Crypto',
  domain: 'https://example.com',
  description: 'Welcome to the Get Crypto.',
} as const;

export default DefaultMetaSettings;
