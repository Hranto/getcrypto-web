const HomeMetaSettings = {
  title: 'Home',
  description:
    'Home - you can see a list of top 10 available currencies from CoinGecko with their latest price.',
} as const;

export default HomeMetaSettings;
