const EnvVars = {
  IsServer: process.env.NEXT_IS_SERVER,
  IsProduction: process.env.NODE_ENV === 'production',
} as const;

export default EnvVars;
