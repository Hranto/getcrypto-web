import Routes from './routes';

export type RouteKeys = keyof typeof Routes;

export type RouteValues = typeof Routes[RouteKeys];
