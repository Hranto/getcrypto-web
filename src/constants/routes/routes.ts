const Routes = {
  Home: '/',
  Error: '/404',
  CoinId: '/coin',
  Coin: '/api/coin',
  Coins: '/api/coins',
  CoinChart: '/api/coin-chart',
} as const;

export default Routes;
