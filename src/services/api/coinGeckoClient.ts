import {CoinGeckoClient} from 'coingecko-api-v3';

const coinGeckoClient = new CoinGeckoClient({
  timeout: 10000,
  autoRetry: true,
});

export default coinGeckoClient;
