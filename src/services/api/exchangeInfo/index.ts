import {CoinMarket, CoinFullInfo} from 'coingecko-api-v3';
import {CoinMarketChartResponse} from 'coingecko-api-v3/dist/Inteface';

import {Routes} from '~/constants';
import {apiClient} from '~/services';

const getCoin = async (id: string): Promise<CoinFullInfo> => {
  try {
    const response = await apiClient.get(Routes.Coin, {
      params: {
        id,
      },
    });

    return response.data as CoinFullInfo;
  } catch {
    throw new Error('Not found');
  }
};

const getCoins = async (): Promise<CoinMarket[]> => {
  const response = await apiClient.get(Routes.Coins);

  return response.data as CoinMarket[];
};

const getCoinChart = async (
  id: string,
  days: number,
): Promise<CoinMarketChartResponse> => {
  try {
    const response = await apiClient.get(Routes.CoinChart, {
      params: {
        id,
        days,
      },
    });

    return response.data as CoinMarketChartResponse;
  } catch {
    throw new Error('Not found');
  }
};

const ExchangeInfoService = {
  getCoin,
  getCoins,
  getCoinChart,
};

export default ExchangeInfoService;
