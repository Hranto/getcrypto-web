import axios from 'axios';

import {EnvVars} from '~/constants';

const defaultOptions = {
  baseURL: !EnvVars.IsProduction
    ? 'http://localhost:3000'
    : 'https://getcrypto.vercel.app',
};

const apiClient = axios.create(defaultOptions);

export default apiClient;
