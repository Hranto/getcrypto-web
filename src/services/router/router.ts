import Router from 'next/router';

import {Routes} from '~/constants';
import {RouteValues} from '~/constants/routes';

const RouterService = {
  push: async (route: RouteValues): Promise<boolean> => {
    return await Router.push(route);
  },
  pushError: async (): Promise<boolean> => {
    return await Router.push(Routes.Error);
  },
};

export default RouterService;
