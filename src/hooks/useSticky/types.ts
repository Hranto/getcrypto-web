import React from 'react';

export interface IUseStickyReturn<T> {
  isSticky: boolean;
  stickyContainerRef: React.Ref<T>;
}
