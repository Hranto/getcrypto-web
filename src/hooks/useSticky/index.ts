import {useState, useEffect, useRef, useCallback} from 'react';

import {IUseStickyReturn} from './types';

const useSticky = <T extends HTMLElement>(
  defaultSticky = false,
): IUseStickyReturn<T> => {
  const stickyContainerRef = useRef<T | null>(null);
  const [isSticky, setIsSticky] = useState(defaultSticky);

  const toggleSticky = useCallback(
    (params) => {
      if (params) {
        const {top} = params;

        if (top <= 40) {
          !isSticky && setIsSticky(true);
        } else {
          isSticky && setIsSticky(false);
        }
      }
    },
    [isSticky],
  );

  useEffect(() => {
    const handleScroll = () => {
      toggleSticky(stickyContainerRef.current?.getBoundingClientRect());
    };

    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, [toggleSticky]);

  return {
    isSticky,
    stickyContainerRef,
  };
};

export default useSticky;
