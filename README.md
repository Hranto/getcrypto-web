# 📝 Get Crypto App

### DEMO 👉 https://getcrypto.vercel.app

## 🌄 Project Screenshot

![alt home](https://i.ibb.co/rM2Rxxp/Screen-Shot-2021-10-26-at-00-20-47.png)

## 📚 Stack

- React
- TypeScript
- SSR: Next.js
- Chart: Victory
- UI: modular-scss
- Crypto: CoinGecko
- Redux: redux-toolkit
- Linting: ESLint, Prettier

## 🗂 Folder Structure

```
src
├── assets
├── components
├── constants
├── containers
├── hooks
├── redux
├── services
├── styles
```

## ➕ Installation and Setup Instructions

#### Example:

Clone down this repository. You will need `node` and `yarn` installed globally on your machine.

Installation:

`yarn`

To Start App:

`yarn start`

To Visit App:

`http://localhost:3000`
